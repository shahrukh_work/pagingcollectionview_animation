//
//  AddNewCardsViewController.swift
//  MultiAnimation
//
//  Created by MAC on 12/01/2022.
//

import UIKit
import CollectionViewPagingLayout

class AddNewCardsViewController: BaseViewController {

    // MARK: - Outlets -
    
    @IBOutlet weak var collectionViewAnimated: CustomCollectionView!
    @IBOutlet weak var mainHolderView: UIView!
    
    // MARK: - Variables -
    
    var dataSource =  [CardData]()
    var dataSourceImages = [UIImage(named: "Exerice"), UIImage(named: "football"), UIImage(named: "Skats"), UIImage(named: "yoga")]
    var dataSourceName = ["Exerice", "Foot Ball", "Skats", "yoga"]
    var indexPath = IndexPath()
    
    
    // MARK: - Controller's LifeCycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionViewAnimated.register(UINib(nibName: "PickCardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PickCardCollectionViewCell")
        mainHolderView.addGradient(colors: [UIColor(hexString: "#A143FF").cgColor, UIColor(hexString: "#348AFF").cgColor])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        for i in (0 ..< dataSourceImages.count) {
            let name = dataSourceName[i]
            let image = dataSourceImages[i]
            let data = CardData(image!, className: name)
            dataSource.append(data)
        }
        
        self.collectionViewAnimated.reloadData()
        
    }
    
    // MARK: - Actions -
    
    @IBAction func addButtonAction(_ sender: Any)
    {
//        if indexPath.item > 0 {
//            let movableIndexPath = IndexPath(item: indexPath.item - 1, section: 0)
//            self.collectionViewAnimated.selectItem(at: movableIndexPath, animated: true, scrollPosition: .left)
//        }
//        if dataSource.count < dataSourceName.count {
//
//            let index = IndexPath(item: dataSource.count - 1, section: 0)
//            let name = dataSourceName[dataSource.count - 1]
//            let image = dataSourceImages[dataSource.count - 1]
//            let data = CardData(image!, className: name)
//            dataSource.append(data)
//            self.collectionViewAnimated.reloadData()
//
//            DispatchQueue.main.async {
//                //self.collectionViewAnimated.scrollToItem(at: index, at: .right, animated: true)
//                self.collectionViewAnimated.selectItem(at: index, animated: true, scrollPosition: .right)
//            }
//
//        }

    }
}

extension AddNewCardsViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PickCardCollectionViewCell", for: indexPath) as! PickCardCollectionViewCell
        cell.cardsImage.image = dataSource[indexPath.item].image
        cell.cardsTextLabel.text = dataSource[indexPath.item].name
    
        return cell
    }
}

extension AddNewCardsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        coordinator?.FruitesPage()
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let index : Int = Int(collectionViewAnimated.contentOffset.x / self.collectionViewAnimated.frame.size.width)
        
        indexPath = IndexPath(item: index, section: 0)
        print(index)
        
    }
}

class CardData : NSObject {
    
    var image = UIImage()
    var name = ""
    
    override init() {
        super.init()
    }
    
    init(_ classIMage: UIImage, className : String) {
        
        self.image = classIMage
        self.name = className
    }
}
