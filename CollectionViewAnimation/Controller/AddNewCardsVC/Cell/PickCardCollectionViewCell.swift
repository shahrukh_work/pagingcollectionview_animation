//
//  PickCardCollectionViewCell.swift
//  MultiAnimation
//
//  Created by MAC on 12/01/2022.
//

import UIKit
import CollectionViewPagingLayout

protocol PickCardCollectionViewCellDelegate {
    func didEnd()
}
class PickCardCollectionViewCell: StackTransformationCollectionViewCell {

    // MARK: - Outlets -
    
    @IBOutlet weak var cardsImage: UIImageView!
    @IBOutlet weak var cardsTextLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var constTralingHolderView: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    
    
}














    
//    var panGestureRecognizer = UIPanGestureRecognizer()
//    var xFromCenter : CGFloat = 0.0
//    var yFromCenter : CGFloat = 0.0
//    var originalPoint = CGPoint()
//    var currentPositionTouched : CGPoint?
//    let stength : CGFloat = 4
//    let range : CGFloat = 0.90
//    let theresoldMargin = (UIScreen.main.bounds.size.width/2) * 0.75
//    var delegate : PickCardCollectionViewCellDelegate?
//
//
//    func setUpGesture()
//    {
//        self.holderView.alpha = 1
//        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.beingDragged(_:)))
//        self.holderView.isUserInteractionEnabled = true
//        self.holderView.addGestureRecognizer(panGestureRecognizer)
//    }
//
//    @objc func beingDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
//
//        xFromCenter = gestureRecognizer.translation(in: self.holderView).x
//        yFromCenter = gestureRecognizer.translation(in: self.holderView).y
//
//        switch gestureRecognizer.state {
//
//        case .began:
//            originalPoint = self.holderView.center;
//
//            break;
//
//        case .changed:
//            let rotationStrength = min(xFromCenter / UIScreen.main.bounds.size.width , 1)
//            let rotationAngel = .pi/8 * rotationStrength
//            let scale = max(1 - abs(rotationStrength) / stength, range)
//            self.holderView.center = CGPoint(x: originalPoint.x + xFromCenter, y: originalPoint.y + yFromCenter)
//            let transforms = CGAffineTransform(rotationAngle: rotationAngel)
//            let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
//            self.holderView.transform = scaleTransform
//
//            let percentage = (self.holderView.center.x*100.0 / self.center.x)/100.0
//            let alpha = percentage >= 1.0 ? (1.0 - (percentage-1.0)) : percentage
//            self.holderView.alpha = alpha
//
//            break;
//
//        case .ended:
//            afterSwipeAction()
//            break;
//
//        case .possible:break
//        case .cancelled:break
//        case .failed:break
//        }
//    }
//
//    fileprivate func afterSwipeAction() {
//
//        if xFromCenter < -theresoldMargin {
//            cardGoesLeft()
//        }
//
//        else if xFromCenter > theresoldMargin {
//            cardGoesRight()
//        }
//        else {
//
//            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: [], animations: {
//                self.holderView.center = self.originalPoint
//                self.holderView.transform = CGAffineTransform(rotationAngle: 0)
//                self.holderView.alpha = 1
//
//            })
//        }
//    }
//
//    func cardGoesRight() {
//
//
//        let finishPoint = CGPoint(x: frame.size.width*2, y: 2 * yFromCenter + originalPoint.y)
//        UIView.animate(withDuration: 0.5, animations: {
//            self.holderView.center = finishPoint
//        }, completion: {(_) in
//            self.delegate?.didEnd()
//
//        })
//
//    }
//
//    /*
//     * Card goes left method
//     */
//    func cardGoesLeft() {
//
//
//        let finishPoint = CGPoint(x: -frame.size.width*2, y: 2 * yFromCenter + originalPoint.y)
//        UIView.animate(withDuration: 0.5, animations: {
//            self.holderView.center = finishPoint
//        }, completion: {(_) in
//            self.delegate?.didEnd()
//
//        })
//
//    }
//}


