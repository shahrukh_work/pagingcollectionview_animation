//
//  SnapshotTransformationCollectionViewCell.swift
//  CollectionViewAnimation
//
//  Created by MAC on 19/01/2022.
//

import UIKit
import CollectionViewPagingLayout

@IBDesignable
class SnapshotTransformationCollectionViewCell: UICollectionViewCell {
    
    var selectedType = ShapeLayout()
    var snapshotOptionsVal: SnapshotTransformViewOptions = .init()
    
    @IBInspectable
    var tranformation: Int = ShapeLayout.scaleBlur.rawValue {
        
        
        didSet {
            
            selectedType = ShapeLayout(value: self.tranformation)
            
            if let option = selectedType.snapshotOptions {
                snapshotOptionsVal = option
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension SnapshotTransformationCollectionViewCell: SnapshotTransformView {
    
    var snapShoot: SnapshotTransformViewOptions {
        snapshotOptionsVal
    }

    var snapShootableView: UIView {
        self.subviews.first ?? self.contentView
    }
}
