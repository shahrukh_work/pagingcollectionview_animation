//
//  StackTransformationCollectionViewCell.swift
//  CollectionViewAnimation
//
//  Created by MAC on 19/01/2022.
//

import UIKit
import CollectionViewPagingLayout

@IBDesignable
class StackTransformationCollectionViewCell: UICollectionViewCell {
    
    var selectedType = ShapeLayout()
    var stackOptionsVal: StackTransformViewOptions = .init()
    
    @IBInspectable
    var tranformation: Int = ShapeLayout.scaleBlur.rawValue {
        
        
        didSet {
            
            selectedType = ShapeLayout(value: self.tranformation)
            
            if let option = selectedType.stackOptions {
                stackOptionsVal = option
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension StackTransformationCollectionViewCell : StackTransformView {
    
    
    var stackOptions: StackTransformViewOptions {
        stackOptionsVal
    }
    
    var stackableView: UIView {
        self.subviews.first ?? self.contentView
    }
    
}
