//
//  ScaleTransformationCollectionViewCell.swift
//  CollectionViewAnimation
//
//  Created by MAC on 19/01/2022.
//

import UIKit
import CollectionViewPagingLayout

@IBDesignable
class ScaleTransformationCollectionViewCell: UICollectionViewCell {
    
    var selectedType = ShapeLayout()

    var scaleOptionsVal: ScaleTransformViewOptions = .init()

    
    @IBInspectable
    var tranformation: Int = ShapeLayout.scaleBlur.rawValue {
        
        
        didSet {
            
            selectedType = ShapeLayout(value: self.tranformation)
            
            if let option = selectedType.scaleOptions {
                scaleOptionsVal = option
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension ScaleTransformationCollectionViewCell : ScaleTransformView {
    
    var scaleOptions: ScaleTransformViewOptions {
        
        scaleOptionsVal
        
    }

    var scalableView: UIView {
        self.subviews.first ?? self.contentView
    }
}
