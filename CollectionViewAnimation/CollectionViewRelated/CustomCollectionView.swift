//
//  CustomCollectionView.swift
//  CollectionViewAnimation
//
//  Created by MAC on 19/01/2022.
//

import UIKit
import CollectionViewPagingLayout

class CustomCollectionView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.isPagingEnabled = true
        let layout = CollectionViewPagingLayout()
        layout.numberOfVisibleItems = 6
        self.collectionViewLayout = layout
        self.showsHorizontalScrollIndicator = false
        self.clipsToBounds = false
        self.backgroundColor = .clear
    }

}
