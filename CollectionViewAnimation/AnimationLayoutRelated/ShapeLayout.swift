//
//  ShapeLayout.swift
//  CollectionViewPagingLayout
//
//  Created by Amir Khorsandi on 2/16/20.
//  Copyright © 2020 Amir Khorsandi. All rights reserved.
//

import UIKit
import CollectionViewPagingLayout

enum ShapeLayout : Int {
    
    case scaleInvertedCylinder = 0//"scaleInvertedCylinder"
    case scaleCylinder = 1 //"scaleCylinder"
    case scaleCoverFlow = 2 //"scaleCoverFlow"
    case scaleRotary = 3 //"scaleRotary"
    case scaleLinear = 4 //"scaleLinear"
    case scaleEaseIn = 5 //"scaleEaseIn"
    case scaleEaseOut = 6 //"scaleEaseOut"
    case scaleBlur = 7 //"scaleBlur"
    
    case stackTransparent = 11 //"stackTransparent"
    case stackPerspective = 12 //"stackPerspective"
    case stackRotary = 13 //"stackRotary"
    case stackVortex = 14 //"stackVortex"
    case stackReverse = 15 //"stackReverse"
    case stackBlur = 16 //"stackBlur"
    
    case snapshotGrid = 21 //"snapshotGrid"
    case snapshotSpace = 22 //"snapshotSpace"
    case snapshotChess = 23 //"snapshotChess"
    case snapshotTiles = 24 //"snapshotTiles"
    case snapshotLines = 25 //"snapshotLines"
    case snapshotBars = 26 //"snapshotBars"
    case snapshotPuzzle = 27 //"snapshotPuzzle"
    case snapshotFade = 28 //"snapshotFade"
    
    public init() {
        self = .scaleInvertedCylinder
    }
    
    public init(value: Int) {
        var result : ShapeLayout = ShapeLayout()
        
        switch value {
            
        case ShapeLayout.scaleInvertedCylinder.rawValue:
            result = ShapeLayout.scaleInvertedCylinder
        
            
        case ShapeLayout.scaleCylinder.rawValue:
            result = ShapeLayout.scaleCylinder
            
        case ShapeLayout.scaleCoverFlow.rawValue:
            result = ShapeLayout.scaleCoverFlow
            
        case ShapeLayout.scaleRotary.rawValue:
            result = ShapeLayout.scaleRotary
            
        case ShapeLayout.scaleLinear.rawValue:
            result = ShapeLayout.scaleLinear
            
        case ShapeLayout.scaleEaseIn.rawValue:
            result = ShapeLayout.scaleEaseIn
            
        case ShapeLayout.scaleEaseOut.rawValue:
            result = ShapeLayout.scaleEaseOut
            
        case ShapeLayout.scaleBlur.rawValue:
            result = ShapeLayout.scaleBlur
            
        case ShapeLayout.stackTransparent.rawValue:
            result = ShapeLayout.stackTransparent
            
        case ShapeLayout.stackPerspective.rawValue:
            result = ShapeLayout.stackPerspective
            
        case ShapeLayout.stackRotary.rawValue:
            result = ShapeLayout.stackRotary
            
        case ShapeLayout.stackVortex.rawValue:
            result = ShapeLayout.stackVortex
            
        case ShapeLayout.stackReverse.rawValue:
            result = ShapeLayout.stackReverse
            
        case ShapeLayout.stackBlur.rawValue:
            result = ShapeLayout.stackBlur
            
        case ShapeLayout.snapshotGrid.rawValue:
            result = ShapeLayout.snapshotGrid
            
        case ShapeLayout.snapshotSpace.rawValue:
            result = ShapeLayout.snapshotSpace
            
        case ShapeLayout.snapshotChess.rawValue:
            result = ShapeLayout.snapshotChess
            
        case ShapeLayout.snapshotTiles.rawValue:
            result = ShapeLayout.snapshotTiles
            
        case ShapeLayout.snapshotLines.rawValue:
            result = ShapeLayout.snapshotLines
            
        case ShapeLayout.snapshotBars.rawValue:
            result = ShapeLayout.snapshotBars
            
        case ShapeLayout.snapshotPuzzle.rawValue:
            result = ShapeLayout.snapshotPuzzle
            
        default:
            result = ShapeLayout.snapshotFade
        }
        
        self = result
    }
}

extension ShapeLayout {
    static let scaleLayouts: [ShapeLayout] = [
        .scaleInvertedCylinder,
        .scaleCylinder,
        .scaleCoverFlow,
        .scaleRotary,
        .scaleLinear,
        .scaleEaseIn,
        .scaleEaseOut,
        .scaleBlur
    ]
    
    static let stackLayouts: [ShapeLayout] = [
        .stackVortex,
        .stackRotary,
        .stackTransparent,
        .stackBlur,
        .stackReverse,
        .stackPerspective
    ]
    
    static let snapshotLayouts: [ShapeLayout] = [
        .snapshotBars,
        .snapshotFade,
        .snapshotGrid,
        .snapshotChess,
        .snapshotLines,
        .snapshotSpace,
        .snapshotTiles,
        .snapshotPuzzle
    ]
}

extension Array where Element == ShapeLayout {
    static var scale: [ShapeLayout] { ShapeLayout.scaleLayouts }
    static var stack: [ShapeLayout] { ShapeLayout.stackLayouts }
    static var snapshot: [ShapeLayout] { ShapeLayout.snapshotLayouts }
}

extension ShapeLayout {
    var stackOptions: StackTransformViewOptions? {
        switch self {
        case .stackTransparent:
            return .layout(.transparent)
        case .stackPerspective:
            return .layout(.perspective)
        case .stackRotary:
            return .layout(.rotary)
        case .stackVortex:
            return .layout(.vortex)
        case .stackReverse:
            return .layout(.reverse)
        case .stackBlur:
            return .layout(.blur)
        default:
            return nil
        }
    }
}

extension ShapeLayout {
    var snapshotOptions: SnapshotTransformViewOptions? {
        switch self {
        case .snapshotGrid:
            return .layout(.grid)
        case .snapshotSpace:
            return .layout(.space)
        case .snapshotChess:
            return .layout(.chess)
        case .snapshotTiles:
            return .layout(.tiles)
        case .snapshotLines:
            return .layout(.lines)
        case .snapshotBars:
            return .layout(.bars)
        case .snapshotPuzzle:
            return .layout(.puzzle)
        case .snapshotFade:
            return .layout(.fade)
        default:
            return nil
        }
        
    }
}

extension ShapeLayout {
    var scaleOptions: ScaleTransformViewOptions? {
        switch self {
        case .scaleBlur:
            return .layout(.blur)
        case .scaleLinear:
            return .layout(.linear)
        case .scaleEaseIn:
            return .layout(.easeIn)
        case .scaleEaseOut:
            return .layout(.easeOut)
        case .scaleRotary:
            return .layout(.rotary)
        case .scaleCylinder:
            return .layout(.cylinder)
        case .scaleInvertedCylinder:
            return .layout(.invertedCylinder)
        case .scaleCoverFlow:
            return .layout(.coverFlow)
        default:
            return nil
        }
    }
}
