//
//  AppDelegate.swift
//  Vocab
//
//  Created by HaiDer's Macbook Pro on 26/12/2021.
//

import UIKit
import CoreData
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UIWindowSceneDelegate {
    var coordinator: MainCoordinator?
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 1)
        let navController = UINavigationController()
        coordinator = MainCoordinator(navigationController: navController)
        
        
        //MARK: - UserState
        
        coordinator?.homePage()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        return true
    }
}

