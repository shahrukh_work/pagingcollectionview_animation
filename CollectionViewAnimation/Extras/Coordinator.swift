//
//  Coordinator.swift
//  Vocab
//
//  Created by HaiDer's Macbook Pro on 26/12/2021.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func homePage()
}

class MainCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        navigationController.navigationBar.isHidden = true
    }
   
    
    func homePage() {
        let vc = AddNewCardsViewController(nibName: "AddNewCardsViewController", bundle: nil)
        vc.coordinator = self
        navigationController.setViewControllers([vc], animated: true)
    }
    
    func FruitesPage() {
        
        let vc = FruitsViewController(nibName: "FruitsViewController", bundle: nil)
        vc.coordinator = self
        navigationController.setViewControllers([vc], animated: true)
        
    }
    
    func popVc() {
        navigationController.popViewController(animated: true)
    }
}

//MARK: - StoryBoard Reference
extension UIStoryboard {
    
    class func controller  <T: UIViewController> (storyboardName : String = "Home") -> T {
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: T.className) as! T
    }
}

//MARK: - NSObject
//extension NSObject {
//    class var className: String {
//        return String(describing: self.self)
//    }
//}
